# DocuOC using Yolov5

## Prerequisites

- Docker installed
- If you have NVIDIA graphic card, following this installation to install NVIDIA container runtime to run this project by GPU ([Installation Guide](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html))

## Installation

CPU:

```
docker build . -t yolov5-cpu -f Dockerfile-cpu
```

GPU

```
docker build . -t yolov5-gpu -f Dockerfile
```

## Command

Train example:

- GPU

```sh
$ docker run -it --ipc=host --gpus all -v "$(pwd)"/data:/usr/src/datasets yolov5-gpu  \
        python3 train.py --img 320 --batch 128 --epochs 1200 --data ./data/data_KRRA.yaml --cfg ./models/yolov5x.yaml --weights '' --cache
```

- CPU

```sh
$ docker run --shm-size=8G -it -v "$(pwd)"/data:/app/data yolov5-cpu \
        python3 train.py --img 320 --batch 128 --epochs 1200 --data ./data/data_KRRA.yaml --cfg ./models/yolov5x.yaml --weights '' --name --cache
```

Training:
Note: If you have new data, run from step 1. If you want to test, I already put some data in `train`, `test` and `valid` folder for testing, just run step `3`.

1. Setup data and file train:
- Need to prepare data in the following directory tree format:
```
  name-forlder
      |__train
      |    |__images
      |    |    |__images1.jpg
      |    |    |__images2.png
      |    |    |__.....
      |    |    
      |    |__labels
      |         |__images1.txt
      |         |__images2.txt
      |         |__.....
      |    
      |__test
      |    |__images
      |    |    |__images3.jpg
      |    |    |__images4.png
      |    |    |__.....
      |    |   
      |    |__labels
      |         |__images3.txt
      |         |__images4.txt
      |         |__.....
      |__valid
           |__images
           |    |__images5.jpg
           |    |__images6.png
           |    |__.....
           |   
           |__labels
                |__images5.txt
                |__images6.txt
                |__.....
```
- Create a yaml file with the following sample:
`data_KRRA.yaml`
```
train: ../data/KRRA/train/images #path to train folder
val: ../data/KRRA/valid/images   #path to validation folder
test: ../data/KRRA/test/images   #path to test folder

nc: 6 #have 6 class
names: ["문서종류", "세대주 성명", "현주소", "동거인 이름", "주민번호 앞자리", "주민번호 뒷자리"] #class name
```

2. Copy data:

Method 1: Example name-folder root is KRRA:
- Copy all your train data to `data/KRRA/train` folder
- Copy all your test data to `data/KRRA/test` folder
- Copy all your validation data to `data/KRRA/valid` folder

Method 2: Use scrpit to prepare data:

- If folder have format:
```
name-forlder
     |__images
     |    |__images1.jpg
     |    |__images2.png
     |    |__.....
     |    
     |__labels
          |__images1.txt
          |__images2.txt
          |__.....
``` 
- You can RUN `copy_files_train.py` to automatically divide folders by format:
```
  name-forlder
      |__train
      |    |__images
      |    |    |__images1.jpg
      |    |    |__images2.png
      |    |    |__.....
      |    |    
      |    |__labels
      |         |__images1.txt
      |         |__images2.txt
      |         |__.....
      |    
      |__test
      |    |__images
      |    |    |__images3.jpg
      |    |    |__images4.png
      |    |    |__.....
      |    |   
      |    |__labels
      |         |__images3.txt
      |         |__images4.txt
      |         |__.....
      |__valid
           |__images
           |    |__images5.jpg
           |    |__images6.png
           |    |__.....
           |   
           |__labels
                |__images5.txt
                |__images6.txt
                |__.....
```
3. Run training:

- GPU

```sh
$ docker run -it --ipc=host --gpus all -v "$(pwd)"/data:/usr/src/datasets yolov5-gpu  \
        python3 train.py --img 320 --batch 128 --epochs 1200 --data ./data/data_KRRA.yaml --cfg ./models/yolov5x.yaml --weights '' --cache
```

```sh
usage: docker run -it --ipc=host --gpus all -v "$(pwd)"/data:/usr/src/datasets yolov5-gpu

optional arguments:
  -it                                     Enables interactive mode and connects to the terminal, 
                                          allowing interaction with the container from the host 
                                          machine terminal.

  --ipc=host                              Shares the IPC (Inter-Process Communication) namespace
                                          with the host, enabling processes inside the container 
                                          to access and communicate with external processes on 
                                          the host machine.
                        
  --gpus all                              Enables all GPUs on the host to be used within the 
                                          container, allowing the container to leverage GPU
                                          resources for faster tasks.
                        
  -v "$(pwd)"/data:/usr/src/datasets      Mounts the data directory from the host machine to the
                                          /usr/src/datasets directory inside the container. 
                                          This facilitates data sharing between the host and the
                                          container, in this case, providing necessary data for 
                                          YOLOv5 model training.
                         
  yolov5-gpu                              The name of the Docker image you want to run.
                        
```

```sh
usage: python3 train.py --img 320 --batch 128 --epochs 1200 --data ./data/data_KRRA.yaml --cfg ./models/yolov5x.yaml --weights '' --cache

optional arguments:
  --img 320                               Sets the input image size for training to 320x320 pixels.

  --batch 128                             Defines the batch size for training as 128.

  --epochs 1200                           Specifies the number of training epochs to be 1200.
                                          An epoch is one complete pass through the entire training dataset.

  --data ./data/data_KRRA.yaml            Points to the YAML file containing information about the dataset,
                                          including paths to training and validation data, classes, etc.

  --cfg ./models/yolov5x.yaml             Specifies the YOLOv5 model configuration file. In this case, 
                                          it using the yolov5x variant.

--weights ''                              Initializes the training with no pretrained weights, starting from scratch.

--cache                                   Enables caching during training.
```
NOTE: You can edit `--img` `batch` `--epoch` `--data` `--cfg` to fit your data 

- CPU

```sh
$ docker run --shm-size=8G -it -v "$(pwd)"/data:/app/data yolov5-cpu \
        python3 train.py --img 320 --batch 128 --epochs 1200 --data ./data/data_KRRA.yaml --cfg ./models/yolov5x.yaml --weights '' --name --cache
```

```sh
usage: docker run --shm-size=8G -it -v "$(pwd)"/data:/app/data yolov5-cpu

optional arguments:
  -it                                     Enables interactive mode and connects to the terminal, 
                                          allowing interaction with the container from the host 
                                          machine terminal.

   --shm-size=8G                          Sets the size of the /dev/shm shared memory space inside
                                          the container to 8 gigabytes. This is important for avoiding
                                          certain issues, especially during training.
                        
  -v "$(pwd)"/data:/usr/src/datasets      Mounts the data directory from the host machine to the
                                          /usr/src/datasets directory inside the container. 
                                          This facilitates data sharing between the host and the
                                          container, in this case, providing necessary data for 
                                          YOLOv5 model training.
                         
  yolov5-cpu                              The name of the Docker image you want to run.
                        
```
NOTE: You can edit `--shm-size` to fit your data 

```sh
usage: python3 train.py --img 320 --batch 128 --epochs 1200 --data ./data/data_KRRA.yaml --cfg ./models/yolov5x.yaml --weights '' --cache

optional arguments:
  --img 320                               Sets the input image size for training to 320x320 pixels.

  --batch 128                             Defines the batch size for training as 128.

  --epochs 1200                           Specifies the number of training epochs to be 1200.
                                          An epoch is one complete pass through the entire training dataset.

  --data ./data/data_KRRA.yaml            Points to the YAML file containing information about the dataset,
                                          including paths to training and validation data, classes, etc.

  --cfg ./models/yolov5x.yaml             Specifies the YOLOv5 model configuration file. In this case, 
                                          it using the yolov5x variant.

--weights ''                              Initializes the training with no pretrained weights, starting from scratch.

--cache                                   Enables caching during training.
```
NOTE: You can edit `--img` `batch` `--epoch` `--data` `--cfg` to fit your data 

